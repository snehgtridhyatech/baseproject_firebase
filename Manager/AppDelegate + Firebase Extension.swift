//
//  AppDelegate + Firebase Extension.swift
//  FirebaseBaseProject
//
//  Created by Sneh on 10/18/23.
//

import Foundation
import FirebaseMessaging
import FirebaseCore
import FirebaseAnalytics
import FirebaseCrashlytics
import FirebaseInAppMessaging
import FirebaseDynamicLinks
import UserNotifications


// MARK: - SetUp
extension AppDelegate  {
    
    /// Use this function to set up firebase with functionalities like configuration , notification, analytics, crashlytics.
    func setupFirebase(application: UIApplication,serverKey: String) {
        FirebaseManager.shared.serverKey = serverKey
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        InAppMessaging.inAppMessaging().delegate = self
        Analytics.setAnalyticsCollectionEnabled(true)
        Crashlytics.crashlytics().setCrashlyticsCollectionEnabled(true)
        requestNotificationAuthorization(application: application)
    }
}



// MARK: - Push Notification
extension AppDelegate : UNUserNotificationCenterDelegate , MessagingDelegate {
    
    
    /// Use this function to request permssions for notification
    func requestNotificationAuthorization(application:UIApplication) {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { (granted, error) in
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
            }
        )
        
        self.setNotificationActionButtons()
    }
    
    
    // MARK: - Register notification
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error in didRegisterForRemoteNotificationsWithDeviceToken : \(error.localizedDescription)")
            } else if let token = token {
                print("\nFCM TOKEN from didRegisterForRemoteNotificationsWithDeviceToken : \(token)\n\n")
                FirebaseManager.shared.fcmToken = token
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error in didFailToRegisterForRemoteNotificationsWithError : ",error.localizedDescription)
    }
    
    
    
    // MARK: - Action buttons
    ///Use this function to set action buttons in notification
    func setNotificationActionButtons() {
        
        let deleteAction = UNNotificationAction(
            identifier: "MarkAsRead",
            title: "Mark as read",
            options: [.destructive])
        let authAction = UNNotificationAction(
            identifier: "Send",
            title: "Send",
            options: [.authenticationRequired])
        let category = UNNotificationCategory(
            identifier: "category",
            actions: [authAction,deleteAction],
            intentIdentifiers: [],
            options: [])
        
        
        let deleteAction2 = UNNotificationAction(
            identifier: "MarkAsRead",
            title: "Mark as read2",
            options: [.destructive])
        let authAction2 = UNNotificationAction(
            identifier: "Send",
            title: "Send2",
            options: [.authenticationRequired])
        let category2 = UNNotificationCategory(
            identifier: "category2",
            actions: [authAction2,deleteAction2],
            intentIdentifiers: [],
            options: [])
        
        
        UNUserNotificationCenter.current().setNotificationCategories(
            [category,category2])
    }
    
    
    // MARK: - Handle notifications and action button taps
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.actionIdentifier == "Send" {
            FirebaseManager.shared.sendPushNotification(to: "dIDVcBeilEUQm3K8PExg2Z:APA91bGo8ayUHHBf6NvnXey3VbusIYttowH72aydAf4XyD42QeRAhZrNPUtnrAzMCaGu92B7lw28LauQXsuETr-D0mLGbI7Ua7MmRqnND8SQZ2_Jbm7IeD0_vvkISu1sf6mCCEgQ_lDs",title: "FirebaseBaseProject", body: "Notification from other user") { isSuccess, response in
            }
        }
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("\n\n\nNotification - willPresent : \(notification)\n\n\n")
        completionHandler([.banner, .badge, .sound, .list])
    }
    
}



// MARK: - Generate token
func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
    if let fcmToken = fcmToken {
        FirebaseManager.shared.fcmToken = fcmToken
        print("\nFCM TOKEN : \(fcmToken)\n\n")
    }
}


// MARK: - In-App-Messsage
extension AppDelegate : InAppMessagingDisplayDelegate {
    func messageClicked(_ inAppMessage: InAppMessagingDisplayMessage, with action: InAppMessagingAction) {
        print("In-app message clicked: \(inAppMessage)")
        // TODO: Example for navigate
        /*  if ((action.actionURL?.absoluteString.contains("SecondController")) != nil) {
         if let rootViewController = self.window?.rootViewController {
         let secondController = rootViewController.storyboard?.instantiateViewController(identifier: "SecondViewController") as? SecondViewController
         rootViewController.present(secondController!, animated: false)
         }
         } */
    }
    
    func impressionDetected(for inAppMessage: InAppMessagingDisplayMessage) {
        print("In-app message displayed: \(inAppMessage)")
    }
}



// MARK: - DeepLink using URLScheme
extension AppDelegate {
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        // TODO: Example for navigate
        /*        if let scheme = url.scheme,
         scheme.localizedCaseInsensitiveCompare(Bundle.main.bundleIdentifier ?? "") == .orderedSame,
         let view = url.host {
         
         var parameters: [String: String] = [:]
         URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
         parameters[$0.name] = $0.value
         }
         
         if view == "SecondController" {
         if let rootViewController = self.window?.rootViewController {
         let secondController = rootViewController.storyboard?.instantiateViewController(identifier: "SecondViewController") as? SecondViewController
         secondController?.labelText = "\(view) + \(parameters)"
         rootViewController.present(secondController!, animated: false)
         }
         }
         }*/
        return true
    }
}



// MARK: - DeepLink using Firebase
extension AppDelegate {
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            // TODO:  Example for navigate refer above examples to get data from link
            
            /*            if view == "SecondController" {
             if let rootViewController = self.window?.rootViewController {
             let secondController = rootViewController.storyboard?.instantiateViewController(identifier: "SecondViewController") as? SecondViewController
             secondController?.labelText = "\(view) + \(parameters)"
             rootViewController.present(secondController!, animated: false)
             }
             } */
            return true
        }
        return false
    }
    
    // For iOS 9 and below
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let incomingURL = userActivity.webpageURL {
            let handled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { dynamicLink, error in
                if let dynamicLink = dynamicLink {
                    // TODO:  Example for navigate refer above examples to get data from link
                    
                    /*            if view == "SecondController" {
                     if let rootViewController = self.window?.rootViewController {
                     let secondController = rootViewController.storyboard?.instantiateViewController(identifier: "SecondViewController") as? SecondViewController
                     secondController?.labelText = "\(view) + \(parameters)"
                     rootViewController.present(secondController!, animated: false)
                     }
                     } */
                }
            }
            return handled
        }
        return false
    }
}
