//
//  FirebaseManager.swift
//  FirebaseBaseProject
//
//  Created by Sneh on 10/17/23.
//

import Foundation
import FirebaseRemoteConfig
import FirebaseAnalytics


/// Use this class for sending firebase notification, adding remote configuration and adding analytics log. (Note : Use setUpFirebase function in didFinishLaunchingWithOptions before this)
class FirebaseManager {
    
    // MARK: - Properties
    
    static let shared = FirebaseManager()
    
    /// FCM token from firebase
    var fcmToken = ""
    
    /// Server key of firebase. This is used in sending push notification from app. You will get this from firebase console -> project settings -> cloud messaging
    var serverKey = ""
    
    /// Minimum fetch interval of remote configuration
    var remoteConfigFetchInterval = 0
    
    /// Fetch timeout of remote configuration
    var remoteConfigTimeout = 0
    
    var isRemoteConfigSetUp = false
    
    /// Remote configuration
    fileprivate var remoteConfig : RemoteConfig?
    
    // MARK: - Notification
    
    /// Use this function to send push notification. Set FirebaseManager.shared.serverKey  before using this.
    func sendPushNotification(to fcmToken: String = FirebaseManager.shared.fcmToken, title: String, body: String, sound: String = "default",notificationData : [String:Any] = [:],jsonData: [String:Any]? = nil, _ completion: (@escaping(_ isSuccess: Bool,_ response: Any) -> Void)) {
        
        var json = [String: Any]()
        
        if let jsonData = jsonData {
            json = jsonData
        } else {
            json = [
                "to" : fcmToken,
                "data" : notificationData,
                "notification" : [
                    "body" : body,
                    "title": title,
                    "sound": sound,
                    "click_action":"category"
                ]
            ]
        }
        
        let body = try? JSONSerialization.data(withJSONObject: json)
        if let url = URL(string: "https://fcm.googleapis.com/fcm/send") {
            var request = URLRequest(url: url)
            request.httpMethod = "post"
            request.setValue("Bearer \(FirebaseManager.shared.serverKey)", forHTTPHeaderField: "Authorization")
            request.setValue("application/json", forHTTPHeaderField:"Content-Type")
            request.httpBody = body
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                    if let error = error {
                        completion(false,error.localizedDescription)
                    } else {
                        if let data = data , data.count != 0 {
                            if let jsonResponse = String(data: data, encoding: .utf8) {
                                completion(true,jsonResponse)
                            }
                        }
                    }
                }
            }.resume()
            
        }
    }
    
    
    // MARK: - Analytics
    
    /// Use this function to add realtime analytics logs to firebase console.
    func addAnalyticsLog(eventName: String, parameters:[String:Any]) {
        let updatedParameters = replaceSpacesWithUnderscores(in: parameters)
        Analytics.logEvent(eventName, parameters: updatedParameters as [String : Any])
    }
    
    
    fileprivate func replaceSpacesWithUnderscores(in dictionary: [String: Any]) -> [String: Any] {
        var updatedDictionary: [String: Any] = [:]
        
        for (key, value) in dictionary {
            let updatedKey = key.replacingOccurrences(of: " ", with: "_")
            
            // If the value is a nested dictionary, recursively process it
            if let nestedDictionary = value as? [String: Any] {
                updatedDictionary[updatedKey] = replaceSpacesWithUnderscores(in: nestedDictionary)
            } else {
                // If the value is a string, replace spaces with underscores
                if let stringValue = value as? String {
                    updatedDictionary[updatedKey] = stringValue.replacingOccurrences(of: " ", with: "_")
                } else {
                    // For other types, keep the original value
                    updatedDictionary[updatedKey] = value
                }
            }
        }
        
        return updatedDictionary
    }
    
    
    // MARK: - Remote configuration
    
    /// Use this function for setUp firebase remote configuration
    fileprivate func setUpRemoteConfig() {
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = TimeInterval(remoteConfigFetchInterval)
        settings.fetchTimeout = TimeInterval(remoteConfigTimeout)
        self.remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig?.configSettings = settings
    }
    
    
    /// Use this function to update remote configuration value and get that updated value.
    func updateRemoteConfig(for key: String, value: Any,_ completion: (@escaping(_ isSuccess: Bool,_ response: Any,_ errorMessage: String) -> Void)) {
        self.setUpRemoteConfig()
        self.setRemoteConfig(for: key, value: value)
        self.getRemoteConfig(for: key, completion)
    }
    
    
    /// Use this function to set remote configuration value for key
    func setRemoteConfig(for key: String, value: Any) {
        self.setUpRemoteConfig()
        self.remoteConfig?.setDefaults([key : value as! NSObject])
    }
    
    
    /// Use this function to get remote configuration value for key
    func getRemoteConfig(for key: String, _ completion: (@escaping(_ isSuccess: Bool,_ response: RemoteConfigValue?,_ errorMessage: String) -> Void)) {
        self.setUpRemoteConfig()
        if isRemoteConfigSetUp {
            let value = RemoteConfig.remoteConfig()
                .configValue(forKey: key)
            completion(true,value,"")
        } else {
            RemoteConfig.remoteConfig().fetch { result, error in
                if let error = error {
                    print("Error fetching remote values \(error)")
                    completion(false,nil,error.localizedDescription)
                    return
                }
                
                RemoteConfig.remoteConfig().activate { _, error in
                    if error != nil {
                        completion(false,nil,error?.localizedDescription ?? "")
                        print("Error in activating remote config : \(error?.localizedDescription ?? "")")
                    } else {
                        self.isRemoteConfigSetUp = true
                        let value = RemoteConfig.remoteConfig()
                            .configValue(forKey: key)
                        completion(true,value,"")
                    }
                }
            }
        }
    }
}
