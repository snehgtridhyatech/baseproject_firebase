//
//  NotificationService.swift
//  NotificationExtension
//
//  Created by Sneh on 10/19/23.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        let content = (request.content.mutableCopy() as? UNMutableNotificationContent)
        if let content = content {
            guard let urlImageString = request.content.userInfo["image"] as? String else {
                contentHandler(content)
                return
            }
            
            if let imageUrl = URL(string: urlImageString) {
                guard let imageData = try? Data(contentsOf: imageUrl) else {
                    contentHandler(content)
                    return
                }
                guard let attachment = UNNotificationAttachment.saveImageToDisk(fileIdentifier: "notificationImage.jpg", data: imageData, options: nil) else {
                    contentHandler(content)
                    return
                }
                
                /// Add image in notification
                content.attachments = [attachment]
            }
            
            
            /// Add action button categary in notification
            content.categoryIdentifier = request.content.userInfo["action"] as? String ?? ""
            
            /// Add channel in notification
            content.threadIdentifier = request.content.userInfo["channel"] as? String ?? ""
            
            /// Handle group notifications
            let groupRequest = UNNotificationRequest(identifier: Bundle.main.bundleIdentifier ?? "", content: content, trigger: nil)
            UNUserNotificationCenter.current().add(groupRequest) { (error) in
                if let error = error {
                    print("Error: \(error.localizedDescription)")
                } else {
                    print("Notification scheduled successfully!")
                }
            }
            
            contentHandler(content)
        }
    }
    

}


//MARK: Extension for Notification Attachment
extension UNNotificationAttachment {
    
    static func saveImageToDisk(fileIdentifier: String, data: Data, options: [NSObject : AnyObject]?) -> UNNotificationAttachment? {
        let fileManager = FileManager.default
        let folderName = ProcessInfo.processInfo.globallyUniqueString
        let folderURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(folderName, isDirectory: true)
        
        do {
            try fileManager.createDirectory(at: folderURL!, withIntermediateDirectories: true, attributes: nil)
            let fileURL = folderURL?.appendingPathComponent(fileIdentifier)
            try data.write(to: fileURL!, options: [])
            let attachment = try UNNotificationAttachment(identifier: fileIdentifier, url: fileURL!, options: options)
            return attachment
        } catch let error {
            print("error \(error)")
        }
        
        return nil
    }
}
