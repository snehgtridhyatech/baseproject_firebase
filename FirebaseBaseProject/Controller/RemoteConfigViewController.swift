//
//  RemoteConfigViewController.swift
//  FirebaseBaseProject
//
//  Created by Sneh on 10/27/23.
//

import UIKit

class RemoteConfigViewController: UIViewController {

    @IBOutlet weak var lblRemoteConfig: UILabel!
    @IBOutlet weak var lblGuide: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
    }
   
    
    func setUp() {
        self.lblGuide.text = "Follow this path:\nFirebase Console -> Remote Configuration\nChange default value of \"label\" named and wait for some time. It will reflect on below."
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(changeRemoteConfig), userInfo: nil, repeats: true)
    }
    
    @objc func changeRemoteConfig() {
        FirebaseManager.shared.getRemoteConfig(for: "label") { isSuccess, response , errorMessage in
            if isSuccess {
                let value = response?.stringValue
                DispatchQueue.main.async {
                    self.lblRemoteConfig.text = value
                }
            }
        }
    }


}
