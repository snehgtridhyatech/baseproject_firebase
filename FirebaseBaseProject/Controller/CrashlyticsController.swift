//
//  CrashlyticsController.swift
//  FirebaseBaseProject
//
//  Created by Sneh on 10/27/23.
//

import UIKit

class CrashlyticsController: UIViewController {

    @IBOutlet weak var lblGuide: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
    }

    func setUp() {
        self.lblGuide.text = "Tap the button and reopen app. After that follow the path:\nFirebase Console -> Crashlytics"
    }
    
    @IBAction func buttonCrashClicked(_ sender: UIButton) {
        fatalError("!!!! Test Crash !!!!")
    }
    
}
