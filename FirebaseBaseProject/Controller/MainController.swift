//
//  MainController.swift
//  FirebaseBaseProject
//
//  Created by Sneh on 10/27/23.
//

import UIKit

class MainController: UIViewController {

    @IBOutlet weak var btnAnalytics: UIButton!
    @IBOutlet weak var btnCrashlytics: UIButton!
    @IBOutlet weak var btnRemoteConfig: UIButton!
    @IBOutlet weak var btnPushNotification: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnAnalytics.layer.cornerRadius = 20
        self.btnCrashlytics.layer.cornerRadius = 20
        self.btnRemoteConfig.layer.cornerRadius = 20
        self.btnPushNotification.layer.cornerRadius = 20
    }

    @IBAction func btnPushNotificationClicked(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
        self.present(controller, animated: true)
    }
    
    @IBAction func btnRemoteConfigClicked(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "RemoteConfigViewController") as! RemoteConfigViewController
        self.present(controller, animated: true)
    }
    
    @IBAction func btnCrashlyticsClicked(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CrashlyticsController") as! CrashlyticsController
        self.present(controller, animated: true)
    }
    
    @IBAction func btnAnalyticsClicked(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AnalyticsController") as! AnalyticsController
        self.present(controller, animated: true)
    }
    
}
