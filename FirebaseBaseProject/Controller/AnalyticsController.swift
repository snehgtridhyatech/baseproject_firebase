//
//  AnalyticsController.swift
//  FirebaseBaseProject
//
//  Created by Sneh on 11/2/23.
//

import UIKit

class AnalyticsController: UIViewController {

    @IBOutlet weak var lblGuide: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
    }

    func setUp() {
        self.lblGuide.text = "Tap the button and reopen app. After that follow the path:\nFirebase Console -> Analytics\nCheck log named with \"Analytics test\""
    }
    
    @IBAction func btnAddAnalyticsClicked(_ sender: UIButton) {
        FirebaseManager.shared.addAnalyticsLog(eventName: "Analytics test", parameters: ["ButtonTapped":Date()])
    }
    
}
