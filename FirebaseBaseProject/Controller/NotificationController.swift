//
//  NotificationController.swift
//  FirebaseBaseProject
//
//  Created by Sneh on 10/27/23.
//

import UIKit

class NotificationController: UIViewController {

    @IBOutlet weak var txtBody: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    
    var channel = "Channel"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnNormalNotificationClicked(_ sender: UIButton) {
        if self.txtTitle.text?.trimmingCharacters(in: .whitespaces) != "" && self.txtBody.text?.trimmingCharacters(in: .whitespaces) != "" {
            FirebaseManager.shared.sendPushNotification(title: self.txtTitle.text ?? "", body: self.txtBody.text ?? "") { isSuccess, response in }
        }
    }
    
    @IBAction func btnImageNotificationClicked(_ sender: UIButton) {
        if self.txtTitle.text?.trimmingCharacters(in: .whitespaces) != "" && self.txtBody.text?.trimmingCharacters(in: .whitespaces) != "" {
            
            let jsonData : [String:Any] = [
                "to" : FirebaseManager.shared.fcmToken,
                "data": [
                    "channel": self.channel,
                    "image":"https://picsum.photos/200.jpg"
                ],
                "notification" : [
                    "body" : self.txtBody.text ?? "",
                    "title": self.txtTitle.text ?? "",
                    "sound": "default"
                ],
                "mutable_content": true
            ]
            
            FirebaseManager.shared.sendPushNotification(title: self.txtTitle.text ?? "", body: self.txtBody.text ?? "",jsonData: jsonData) { isSuccess, response in }
        }
    }
    
    @IBAction func btnDifferentChannelNotificationClicked(_ sender: UIButton) {
        if self.txtTitle.text?.trimmingCharacters(in: .whitespaces) != "" && self.txtBody.text?.trimmingCharacters(in: .whitespaces) != "" {
            
            self.channel = "\(self.channel)\(self.channel)"
            
            let jsonData : [String:Any] = [
                "to" : FirebaseManager.shared.fcmToken,
                "data": [
                    "channel": self.channel,
                    "image":"https://picsum.photos/200.jpg"
                ],
                "notification" : [
                    "body" : self.txtBody.text ?? "",
                    "title": self.txtTitle.text ?? "",
                    "sound": "default"
                ],
                "mutable_content": true
            ]
            
            FirebaseManager.shared.sendPushNotification(title: self.txtTitle.text ?? "", body: self.txtBody.text ?? "",jsonData: jsonData) { isSuccess, response in }
        }
    }
    
    @IBAction func btnActionButtonNotificationClicked(_ sender: UIButton) {
        if self.txtTitle.text?.trimmingCharacters(in: .whitespaces) != "" && self.txtBody.text?.trimmingCharacters(in: .whitespaces) != "" {
            
            let jsonData : [String:Any] = [
                "to" : FirebaseManager.shared.fcmToken,
                "data": [
                    "action": "category",
                    "screen": "screen2",
                    "channel": self.channel,
                    "image":"https://picsum.photos/200.jpg"
                ],
                "notification" : [
                    "body" : self.txtBody.text ?? "",
                    "title": self.txtTitle.text ?? "",
                    "sound": "default"
                ],
                "mutable_content": true
            ]
            
            FirebaseManager.shared.sendPushNotification(title: self.txtTitle.text ?? "", body: self.txtBody.text ?? "",jsonData: jsonData) { isSuccess, response in }
        }
    }
}
